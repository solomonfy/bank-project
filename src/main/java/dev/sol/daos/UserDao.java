package dev.sol.daos;

import java.util.List;

import dev.sol.models.*;

public interface UserDao {
	
	
	public List<User> getAllUser();
	public User addNewUser(User user);
	public boolean ChangeUserInfo(User user);
	public boolean removeUserByID(User user);


}
