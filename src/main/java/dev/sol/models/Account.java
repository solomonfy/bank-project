package dev.sol.models;

public class Account {

	private int accountNumber;
	private double initialBalance;
	private double currentBalance;
	private User accountHolder;
	private AccountStatus status;
	
	
	public Account () {
		super();
		
	}
	

	public Account(int accountNumber, double initialBalance, double currentBalance, User accountHolder,
			AccountStatus status) {
		super();
		this.accountNumber = accountNumber;
		this.initialBalance = initialBalance;
		this.currentBalance = currentBalance;
		this.accountHolder = accountHolder;
		this.status = status;
	}




	public static void main(String[] args) {
		
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}


	public double getInitialBalance() {
		return initialBalance;
	}

	public void setInitialBalance(double initialBalance) {
		this.initialBalance = initialBalance;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}


	public User getAccountHolder() {
		return accountHolder;
	}


	public void setAccountHolder(User accountHolder) {
		this.accountHolder = accountHolder;
	}


	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountHolder == null) ? 0 : accountHolder.hashCode());
		result = prime * result + accountNumber;
		long temp;
		temp = Double.doubleToLongBits(currentBalance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(initialBalance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountHolder == null) {
			if (other.accountHolder != null)
				return false;
		} else if (!accountHolder.equals(other.accountHolder))
			return false;
		if (accountNumber != other.accountNumber)
			return false;
		if (Double.doubleToLongBits(currentBalance) != Double.doubleToLongBits(other.currentBalance))
			return false;
		if (Double.doubleToLongBits(initialBalance) != Double.doubleToLongBits(other.initialBalance))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return " Account detail - \n   Account Number: " + accountNumber + ", \n   CurrentBalance: $"
				+ currentBalance + ", \n   Account Holder: " + accountHolder + ", \n   Account status: " + status;
	}
	
}
